﻿using System.Collections.Generic;
using System.Linq;
using WordCombiner.Core.Services;

namespace WordCombiner.Infrastructure.Services {
  public class WordCombinerService : IWordCombinerService {
    public IEnumerable<string> FindCombinations(int wordLength, List<string> wordParts, List<string> words) {
      foreach (var wordPart in wordParts) {
        var appendPartLength = wordLength - wordPart.Length;
        var appendParts = wordParts.Where(wp => wp.Length == appendPartLength);
        foreach (var appendPart in appendParts) {
          var appendWord = $"{wordPart}{appendPart}";
          if (words.Contains(appendWord)) {
            yield return $"{wordPart}+{appendPart}={appendWord}";
          }
        }
      }
    }
  }
}
