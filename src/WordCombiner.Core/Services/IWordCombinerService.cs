﻿using System.Collections.Generic;

namespace WordCombiner.Core.Services {
  public interface IWordCombinerService {
    IEnumerable<string> FindCombinations(int wordLength, List<string> wordParts, List<string> words);
  }
}
