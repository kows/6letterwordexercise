﻿namespace WordCombiner.Core.AppSettings {
  public class WordCombinerOptions {

    public int DefaultWordLength { get; set; }

    public string InputFilePath { get; set; }

  }
}
