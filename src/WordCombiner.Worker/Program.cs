﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace WordCombiner.Worker {
  class Program {
    public static void Main(string[] args) {
      CreateHostBuilder().Build().Run();
    }

    public static IHostBuilder CreateHostBuilder() =>
           Host.CreateDefaultBuilder()
               .ConfigureServices((hostContext, services) => {
                 services.AddHostedService<WordCombinerBackgroundService>();
                 new Startup(hostContext.Configuration).ConfigureServices(services);
               });
  }
}
