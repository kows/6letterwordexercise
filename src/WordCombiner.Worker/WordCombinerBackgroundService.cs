﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WordCombiner.Core.AppSettings;
using WordCombiner.Core.Services;

namespace WordCombiner.Worker {
  public class WordCombinerBackgroundService : BackgroundService {
    private readonly WordCombinerOptions wordCombinerOptions;
    private readonly ILogger<WordCombinerBackgroundService> logger;
    private readonly IWordCombinerService wordCombinerService;

    public WordCombinerBackgroundService(ILogger<WordCombinerBackgroundService> logger, IOptionsMonitor<WordCombinerOptions> wordCombinerOptions, IWordCombinerService wordCombinerService) {
      this.wordCombinerOptions = wordCombinerOptions.CurrentValue;
      this.logger = logger;
      this.wordCombinerService = wordCombinerService;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
      try {
        var lines = LoadLines(wordCombinerOptions.InputFilePath);
        var parts = lines.Where(l => l.Length != wordCombinerOptions.DefaultWordLength);
        var words = lines.Where(l => l.Length == wordCombinerOptions.DefaultWordLength);
        if (parts.Any() ||
            words.Any()) {
          //no results
        }

        var result = wordCombinerService.FindCombinations(wordCombinerOptions.DefaultWordLength, parts.ToList(), words.ToList()).ToList();
        logger.LogInformation(string.Join("\r\n", result));
      }
      catch (Exception ex) {
        //log
      }
    }

    private IEnumerable<string> LoadLines(string inputFilePath) {
      if (!File.Exists(inputFilePath)) {
        //output error  
        return Enumerable.Empty<string>();
      }

      return File.ReadAllLines(inputFilePath);
    }
  }
}
