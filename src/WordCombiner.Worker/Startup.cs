﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WordCombiner.Core.AppSettings;
using WordCombiner.Core.Services;
using WordCombiner.Infrastructure.Services;

namespace WordCombiner.Worker {
  public class Startup {
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration) {
      Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services) {
      services.AddOptions();
      services.Configure<WordCombinerOptions>(Configuration.GetSection(nameof(WordCombinerOptions)));
      services.AddSingleton<IWordCombinerService, WordCombinerService>();
    }
  }
}
