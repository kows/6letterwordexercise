﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordCombiner.Core.Services;
using WordCombiner.Infrastructure.Services;

namespace WordCombiner.Tests {
  [TestClass]
  public class WordCombinerServiceTests {
    private IWordCombinerService wordCombinerService;

    [TestInitialize]
    public void Init() {
      this.wordCombinerService = new WordCombinerService();
    }

    [TestMethod]
    public void WordLength_Checked() {
      var parts = new[] { "or", "ange", "anges" }.ToList();
      var words = new[] { "oranges", "orange" }.ToList();

      var results = wordCombinerService.FindCombinations(6, parts, words);

      Assert.AreEqual(1, results.Count());
      Assert.AreEqual("orange", results.First());
    }

    [TestMethod]
    public void PartOrder_NoInfluence() {
      var parts = new[] { "ange", "or" }.ToList();
      var words = new[] { "orange" }.ToList();

      var results = wordCombinerService.FindCombinations(6, parts, words);

      Assert.AreEqual(1, results.Count());
      Assert.AreEqual("orange", results.First());
    }
  }
}
